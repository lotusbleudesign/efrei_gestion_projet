
## CAF project 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.11.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## API KEY

Api_key in dir environments/environment.ts

### Graph

ngx-charts

### Version 1.2.0
